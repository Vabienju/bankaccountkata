package adapter;

import domain.BankAccount;
import port.outgoing.LoadAccountOutgoing;
import port.outgoing.SaveAccountOutgoing;

import java.util.HashMap;

public class BankAccountRepository implements LoadAccountOutgoing, SaveAccountOutgoing {

    private HashMap<Long, BankAccount> bankRepository = new HashMap<>();

    public BankAccountRepository() {
        bankRepository.put(1L, new BankAccount(1,0));
        bankRepository.put(2L, new BankAccount(2,10));
        bankRepository.put(3L, new BankAccount(3,20));
        bankRepository.put(4L, new BankAccount(4,30));
    }

    public HashMap<Long, BankAccount> getBankRepository() {
        return bankRepository;
    }

    @Override
    public BankAccount load(Long id) {
        return bankRepository.get(id);
    }

    @Override
    public void save(BankAccount bankAccount) {
        bankRepository.replace(bankAccount.getId(), bankAccount);
    }
}
