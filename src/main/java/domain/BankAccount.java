package domain;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BankAccount {

    private long id;
    private long balance;
    private List<String> history = new ArrayList<>();

    public BankAccount(long id, int balance) {
        this.id = id;
        this.balance = balance;
    }

    public long getId() {
        return id;
    }

    public long getBalance() {
        return balance;
    }

    public List<String> getHistory() {
        return history;
    }

    public boolean deposit(long myDeposit){
        if (myDeposit < 0){
            System.out.println("Operation refused");
            return false;
        }
        balance += myDeposit;
        addOperationToHistory(myDeposit, "deposit");
        return true;
    }

    public boolean withdrawal(long myWithdrawal){
        if((balance - myWithdrawal) < 0 || myWithdrawal < 0 ){
            System.out.println("Operation refused");
            return false;
        }

        balance -= myWithdrawal;
        addOperationToHistory(myWithdrawal, "withdrawal");
        return true;
    }

    public void operationsHistoryPrint(){
        for (String historyLine: history) {
            System.out.println(historyLine);
        }
    }

    private void addOperationToHistory(long amount, String operation){
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        String historyLine = "Operation : " + operation + " Date : " + timestamp +  " Amount : " + amount + " Balance : " + balance;
        history.add(historyLine);
    }

}
