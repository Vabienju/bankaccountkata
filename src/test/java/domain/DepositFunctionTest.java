package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DepositFunctionTest {

    BankAccount bankAccount;

    @BeforeEach
    void init(){
        bankAccount = new BankAccount(12, 30);
    }

    @Test
    @DisplayName("Deposit Position")
    void test1() {
        assertEquals(true,bankAccount.deposit(25));
    }

    @Test
    @DisplayName("Deposit Negative")
    void test2() {
        assertEquals(false,bankAccount.deposit(-25));
    }

    @Test
    @DisplayName("Update balance")
    void test3() {
        bankAccount.deposit(25);
        assertEquals(30+25,bankAccount.getBalance());
    }

    @Test
    @DisplayName("Do not update balance negative deposite")
    void test4() {
        bankAccount.deposit(-25);
        assertEquals(30,bankAccount.getBalance());
    }

}
