package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class OperationsHistoryPrintTest {

    BankAccount bankAccount;


    @BeforeEach
    void init(){
        bankAccount = new BankAccount(1, 10);
    }

    @Test
    @DisplayName("Withdrawal smaller than balance")
    void test1() {
        bankAccount.withdrawal(5);
        bankAccount.operationsHistoryPrint();
    }

    @Test
    @DisplayName("Withdrawal negative")
    void test2() {
        bankAccount.withdrawal(-5);
        bankAccount.operationsHistoryPrint();
    }

    @Test
    @DisplayName("Withdrawal equal to balance")
    void test3() {
        bankAccount.withdrawal(10);
        bankAccount.operationsHistoryPrint();
    }

    @Test
    @DisplayName("Deposit Positive")
    void test4() {
        bankAccount.deposit(5);
        bankAccount.operationsHistoryPrint();
    }

    @Test
    @DisplayName("Deposit Negative")
    void test5() {
        bankAccount.deposit(-5);
        bankAccount.operationsHistoryPrint();
    }

    @Test
    @DisplayName("Multiple Operations")
    void test6() {
        bankAccount.deposit(5);
        bankAccount.withdrawal(5);
        bankAccount.operationsHistoryPrint();
    }


    @Test
    @DisplayName("Multiple Operations with deposit not allow")
    void test7() {
        bankAccount.deposit(-5);
        bankAccount.withdrawal(5);
        bankAccount.operationsHistoryPrint();
    }

    @Test
    @DisplayName("Multiple Operations with withdrawal not allow")
    void test8() {
        bankAccount.deposit(5);
        bankAccount.withdrawal(-5);
        bankAccount.operationsHistoryPrint();
    }

}
