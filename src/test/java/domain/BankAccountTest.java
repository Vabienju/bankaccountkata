package domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BankAccountTest {

    @Test
    @DisplayName("Initialize BankAccount Class")
    public void test1(){
        BankAccount bankAccount = new BankAccount(1,0);
        assertEquals(1L, bankAccount.getId());
        assertEquals(0, bankAccount.getBalance());
    }


}
