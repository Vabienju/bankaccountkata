package adapter;

import domain.BankAccount;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class BankAccountRepositoryTest {

    @Test
    public void test(){
        BankAccountRepository bankAccountRepository = new BankAccountRepository();
        HashMap<Long, BankAccount> expected = new HashMap<>();
        expected.put(1L, new BankAccount(1,0));
        expected.put(2L, new BankAccount(2,10));
        expected.put(3L, new BankAccount(3,20));
        expected.put(4L, new BankAccount(4,30));

        for(Map.Entry<Long, BankAccount> value: expected.entrySet()){
            BankAccount actualValue = bankAccountRepository.getBankRepository().get(value.getKey());
            assertNotNull(actualValue);
            assertEquals(value.getValue().getId(), actualValue.getId());
            assertEquals(value.getValue().getBalance(), actualValue.getBalance());
            assertTrue(value.getValue().getHistory().equals(actualValue.getHistory()));
        }


    }


}
