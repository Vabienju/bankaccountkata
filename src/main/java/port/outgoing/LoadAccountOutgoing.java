package port.outgoing;

import domain.BankAccount;

public interface LoadAccountOutgoing {
    BankAccount load(Long id);
}
