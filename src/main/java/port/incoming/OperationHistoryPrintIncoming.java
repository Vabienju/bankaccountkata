package port.incoming;

public interface OperationHistoryPrintIncoming {
    public void operationsHistoryPrint(Long id);
}
