package adapter;

import domain.BankAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LoadTest {

    BankAccountRepository bankAccountRepository;

    @BeforeEach
    void init(){
        bankAccountRepository = new BankAccountRepository();
    }

    @Test
    @DisplayName("Get existing Account")
    public void test1(){
        BankAccount expected = new BankAccount(1L, 0);
        assertEquals(expected.getId(),bankAccountRepository.load(1L).getId() );
        assertEquals(expected.getBalance(),bankAccountRepository.load(1L).getBalance() );
        assertTrue(expected.getHistory().equals(bankAccountRepository.load(1L).getHistory()));
    }

    @Test
    @DisplayName("Get existing Account")
    public void test2(){
        assertEquals(null, bankAccountRepository.load(25L));

    }

}
