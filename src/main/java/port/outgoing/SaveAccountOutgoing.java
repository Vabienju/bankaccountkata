package port.outgoing;

import domain.BankAccount;

public interface SaveAccountOutgoing {

    void save(BankAccount bankAccount);
}
