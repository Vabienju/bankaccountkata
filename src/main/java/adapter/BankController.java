package adapter;

import port.incoming.DepositIncoming;
import port.incoming.OperationHistoryPrintIncoming;
import port.incoming.WithdrawIncoming;

public class BankController {

    private DepositIncoming depositIncoming;
    private WithdrawIncoming withdrawIncoming;
    private OperationHistoryPrintIncoming operationHistoryPrintIncoming;

    public BankController(DepositIncoming depositIncoming, WithdrawIncoming withdrawIncoming, OperationHistoryPrintIncoming operationHistoryPrintIncoming) {
        this.depositIncoming = depositIncoming;
        this.withdrawIncoming = withdrawIncoming;
        this.operationHistoryPrintIncoming = operationHistoryPrintIncoming;
    }

    public void deposit(long id, long myDeposit){
        depositIncoming.deposit(id, myDeposit);
    }

    public void withdraw (long id, long myWithdraw){
        withdrawIncoming.withdraw(id, myWithdraw);
    }

    public void operationHistoryPrint(long id){
        operationHistoryPrintIncoming.operationsHistoryPrint(id);
    }
}
