package adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import service.BankAccountService;

public class CallFunctionTest {

    BankAccountRepository repository;
    BankAccountService service;
    BankController bankController;

    @BeforeEach
    void init(){
        repository = new BankAccountRepository();
        service = new BankAccountService(repository,repository);
        bankController = new BankController(service,service,service);
    }

    @Test
    @DisplayName("Print history Account exist")
    public void test1(){
        bankController.operationHistoryPrint(1L);
    }

    @Test
    @DisplayName("Print history Account do not exist")
    public void test2(){
        bankController.operationHistoryPrint(25L);
    }

    @Test
    @DisplayName("Positive deposit")
    public void test3(){
        bankController.deposit(1L, 5);
        bankController.operationHistoryPrint(1L);
    }

    @Test
    @DisplayName("Negative deposit")
    public void test4(){
        bankController.deposit(1L, -5);
        bankController.operationHistoryPrint(1L);
    }

    @Test
    @DisplayName("Account for deposit do not exist")
    public void test5(){
        bankController.deposit(25L, -5);
    }

    @Test
    @DisplayName("Positive withdraw higher than balance")
    public void test6(){
        bankController.withdraw(1L, 5);
        bankController.operationHistoryPrint(1L);
    }

    @Test
    @DisplayName("Negative withdraw")
    public void test7(){
        bankController.withdraw(1L, -5);
        bankController.operationHistoryPrint(1L);
    }

    @Test
    @DisplayName("Positive withdraw smaller than balance")
    public void test8(){
        bankController.withdraw(2L, 1);
        bankController.operationHistoryPrint(2L);
    }

    @Test
    @DisplayName("Account for withdraw do not exist")
    public void test9(){
        bankController.withdraw(25L, -5);
    }

}
