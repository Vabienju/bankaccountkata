package adapter;

import domain.BankAccount;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class SaveTest {
    BankAccountRepository bankAccountRepository;
    HashMap<Long, BankAccount> expected;

    @BeforeEach
    void init(){
        bankAccountRepository = new BankAccountRepository();
        expected = new HashMap<>();
        expected.put(1L, new BankAccount(1,0));
        expected.put(2L, new BankAccount(2,10));
        expected.put(3L, new BankAccount(3,20));
        expected.put(4L, new BankAccount(4,30));
    }

    @Test
    @DisplayName("Save existing Account")
    public void test1(){
        BankAccount updateAccount = new BankAccount(1L, 20);
        bankAccountRepository.save(updateAccount);

        assertEquals(updateAccount.getId(),bankAccountRepository.load(1L).getId() );
        assertEquals(updateAccount.getBalance(),bankAccountRepository.load(1L).getBalance() );
        assertTrue(updateAccount.getHistory().equals(bankAccountRepository.load(1L).getHistory()));

    }

    @Test
    @DisplayName("Get existing Account")
    public void test2(){
        BankAccount updateAccount = new BankAccount(25L, 20);
        bankAccountRepository.save(updateAccount);

        for(Map.Entry<Long, BankAccount> value: expected.entrySet()){
            BankAccount actualValue = bankAccountRepository.getBankRepository().get(value.getKey());
            assertNotNull(actualValue);
            assertEquals(value.getValue().getId(), actualValue.getId());
            assertEquals(value.getValue().getBalance(), actualValue.getBalance());
            assertTrue(value.getValue().getHistory().equals(actualValue.getHistory()));
        }

    }

}
