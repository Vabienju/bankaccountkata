package service;

import adapter.BankAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import port.outgoing.LoadAccountOutgoing;
import port.outgoing.SaveAccountOutgoing;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OperationHistoryPrintTest {


    BankAccountService bankAccountService;

    @BeforeEach
    void init() {
        LoadAccountOutgoing loadAccountOutgoing = new BankAccountRepository();
        SaveAccountOutgoing saveAccountOutgoing = new BankAccountRepository();
        bankAccountService = new BankAccountService(loadAccountOutgoing, saveAccountOutgoing);
    }

    @Test
    @DisplayName("Print history when account exist")
    public void test1() {
        bankAccountService.deposit(1L, 5);
        bankAccountService.withdraw(1L, 5);
        bankAccountService.operationsHistoryPrint(1L);
    }

    @Test
    @DisplayName("Print no Account when account exist")
    public void test2() {
        bankAccountService.operationsHistoryPrint(25L);
    }
}
