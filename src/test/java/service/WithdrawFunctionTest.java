package service;

import adapter.BankAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import port.outgoing.LoadAccountOutgoing;
import port.outgoing.SaveAccountOutgoing;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WithdrawFunctionTest {

    BankAccountService bankAccountService;

    @BeforeEach
    void init(){
        LoadAccountOutgoing loadAccountOutgoing = new BankAccountRepository();
        SaveAccountOutgoing saveAccountOutgoing = new BankAccountRepository();
        bankAccountService = new BankAccountService(loadAccountOutgoing, saveAccountOutgoing);
    }

    @Test
    @DisplayName("withdraw positive higher than balance")
    public void test1(){
        assertEquals(false, bankAccountService.withdraw(1L,5));
    }

    @Test
    @DisplayName("withdraw Negative")
    public void test2(){
        assertEquals(false, bankAccountService.withdraw(1L,-5));
    }

    @Test
    @DisplayName("withdraw smaller than balance")
    public void test3(){
        assertEquals(true, bankAccountService.withdraw(2L,5));
    }


    @Test
    @DisplayName("withdraw with wrong id Account")
    public void test4(){
        assertEquals(false, bankAccountService.withdraw(25L,-5));
    }

}
