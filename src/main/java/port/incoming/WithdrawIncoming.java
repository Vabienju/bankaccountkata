package port.incoming;

public interface WithdrawIncoming {
    boolean withdraw(Long id, long myWithdrawal);
}
