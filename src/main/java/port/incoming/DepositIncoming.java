package port.incoming;

public interface DepositIncoming {
    boolean deposit(Long id, long myDeposit);
}
