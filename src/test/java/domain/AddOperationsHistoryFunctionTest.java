package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AddOperationsHistoryFunctionTest {

    BankAccount bankAccount;
    List<String> expected;


    @BeforeEach
    void init(){
        bankAccount = new BankAccount(1, 10);
        expected = new ArrayList<>();
    }

    @Test
    @DisplayName("Withdrawal smaller than balance")
    void test1() {
        bankAccount.withdrawal(5);
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        expected.add("Operation : withdrawal Date : " + timestamp +  " Amount : 5 Balance : 5");
        assertTrue(expected.equals(bankAccount.getHistory()));
    }

    @Test
    @DisplayName("Withdrawal negative")
    void test2() {
        bankAccount.withdrawal(-5);
        assertTrue(expected.equals(bankAccount.getHistory()));
    }

    @Test
    @DisplayName("Withdrawal equal to balance")
    void test3() {
        bankAccount.withdrawal(10);
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        expected.add("Operation : withdrawal Date : " + timestamp +  " Amount : 10 Balance : 0");
        assertTrue(expected.equals(bankAccount.getHistory()));
    }

    @Test
    @DisplayName("Deposit Positive")
    void test4() {
        bankAccount.deposit(5);
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        expected.add("Operation : deposit Date : " + timestamp +  " Amount : 5 Balance : 15");
        assertTrue(expected.equals(bankAccount.getHistory()));
    }

    @Test
    @DisplayName("Deposit Negative")
    void test5() {
        bankAccount.deposit(-5);
        assertTrue(expected.equals(bankAccount.getHistory()));
    }

    @Test
    @DisplayName("Multiple Operations")
    void test6() {
        bankAccount.deposit(5);
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        expected.add("Operation : deposit Date : " + timestamp +  " Amount : 5 Balance : 15");
        bankAccount.withdrawal(5);
        Long timeMillis2  = System.currentTimeMillis();
        Long datetime2 = TimeUnit.MILLISECONDS.toSeconds(timeMillis2);
        Timestamp timestamp2 = new Timestamp(datetime2);
        expected.add("Operation : withdrawal Date : " + timestamp2 +  " Amount : 5 Balance : 10");
        assertTrue(expected.equals(bankAccount.getHistory()));
    }


    @Test
    @DisplayName("Multiple Operations with deposit not allow")
    void test7() {
        bankAccount.deposit(-5);
        bankAccount.withdrawal(5);
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        expected.add("Operation : withdrawal Date : " + timestamp +  " Amount : 5 Balance : 5");
        assertTrue(expected.equals(bankAccount.getHistory()));
    }

    @Test
    @DisplayName("Multiple Operations with withdrawal not allow")
    void test8() {
        bankAccount.deposit(5);
        Long timeMillis  = System.currentTimeMillis();
        Long datetime = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        Timestamp timestamp = new Timestamp(datetime);
        expected.add("Operation : deposit Date : " + timestamp +  " Amount : 5 Balance : 15");
        bankAccount.withdrawal(-5);
        assertTrue(expected.equals(bankAccount.getHistory()));
    }
}
