package service;

import adapter.BankAccountRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import port.outgoing.LoadAccountOutgoing;
import port.outgoing.SaveAccountOutgoing;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DepositFunctionTest {

    BankAccountService bankAccountService;

    @BeforeEach
    void init(){
        LoadAccountOutgoing loadAccountOutgoing = new BankAccountRepository();
        SaveAccountOutgoing saveAccountOutgoing = new BankAccountRepository();
        bankAccountService = new BankAccountService(loadAccountOutgoing, saveAccountOutgoing);
    }

    @Test
    @DisplayName("Deposit positive")
    public void test1(){
        assertEquals(true, bankAccountService.deposit(1L,5));
    }

    @Test
    @DisplayName("Deposit Negative")
    public void test2(){
        assertEquals(false, bankAccountService.deposit(1L,-5));
    }

    @Test
    @DisplayName("Deposit with wrong id Account")
    public void test3(){
        assertEquals(false, bankAccountService.deposit(25L,-5));
    }
}
