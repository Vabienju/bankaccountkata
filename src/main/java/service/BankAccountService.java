package service;

import domain.BankAccount;
import port.incoming.DepositIncoming;
import port.incoming.OperationHistoryPrintIncoming;
import port.incoming.WithdrawIncoming;
import port.outgoing.LoadAccountOutgoing;
import port.outgoing.SaveAccountOutgoing;

public class BankAccountService  implements DepositIncoming, WithdrawIncoming, OperationHistoryPrintIncoming {

    private LoadAccountOutgoing loadAccountOutgoing;
    private SaveAccountOutgoing saveAccountOutgoing;

    public BankAccountService(LoadAccountOutgoing loadAccountOutgoing, SaveAccountOutgoing saveAccountOutgoing) {
        this.loadAccountOutgoing = loadAccountOutgoing;
        this.saveAccountOutgoing = saveAccountOutgoing;
    }

    @Override
    public boolean deposit(Long id, long myDeposit) {
        BankAccount account;
        boolean result;
        try {
            account = loadAccountOutgoing.load(id);
            result = account.deposit(myDeposit);
        } catch (NullPointerException e){
            System.out.println("Operation refused");
            return false;
        }
        if (result)
            saveAccountOutgoing.save(account);
        else
            return false;
        return true;
    }

    @Override
    public boolean withdraw(Long id, long myWithdrawal) {
        BankAccount account;
        boolean result;
        try {
            account = loadAccountOutgoing.load(id);
            result = account.withdrawal(myWithdrawal);
        }catch (NullPointerException e){
            System.out.println("Operation refused");
            return false;
        }

        if (result)
            saveAccountOutgoing.save(account);
        else
            return false;
        return true;
    }

    @Override
    public void operationsHistoryPrint(Long id) {
        try {
        BankAccount account = loadAccountOutgoing.load(id);
        account.operationsHistoryPrint();
        } catch (NullPointerException e ){
            System.out.println("The account "+ id + " do not exist");
        }
    }
}
