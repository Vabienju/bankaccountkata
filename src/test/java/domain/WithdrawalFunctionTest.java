package domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WithdrawalFunctionTest {

    BankAccount bankAccount;

    @BeforeEach
    void init(){
        bankAccount = new BankAccount(1, 10);
    }

    @Test
    @DisplayName("Withdrawal smaller than balance")
    void test1() {
        assertEquals(true,bankAccount.withdrawal(5));
    }

    @Test
    @DisplayName("Withdrawal Negative")
    void test2() {
        assertEquals(false,bankAccount.withdrawal(-25));
    }

    @Test
    @DisplayName("Withdrawal higher than balance")
    void test3() {
        assertEquals(false,bankAccount.withdrawal(25));
    }

    @Test
    @DisplayName("Withdrawal equal to balance")
    void test4() {
        assertEquals(true,bankAccount.withdrawal(10));
    }

    @Test
    @DisplayName("Update balance when withdrawal smaller than balance")
    void test5() {
        bankAccount.withdrawal(5);
        assertEquals(10-5,bankAccount.getBalance());
    }

    @Test
    @DisplayName("Do not update balance when withdrawal negative ")
    void test6() {
        bankAccount.withdrawal(-25);
        assertEquals(10,bankAccount.getBalance());
    }

    @Test
    @DisplayName("Do not update balance when withdrawal higher than balance ")
    void test7() {
        bankAccount.withdrawal(25);
        assertEquals(10,bankAccount.getBalance());
    }

    @Test
    @DisplayName("Update balance when withdrawal equal to balance ")
    void test8() {
        bankAccount.withdrawal(10);
        assertEquals(0,bankAccount.getBalance());
    }
}
